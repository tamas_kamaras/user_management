'use strict';

window.onload = ()=> {
  document.querySelectorAll('button[id]')
    .forEach((button)=> {
      button.addEventListener('click', (e)=> {
        let table = document.querySelector(`#${e.target.id} + table`);
        if (table.hasAttribute('hidden')) {
          table.removeAttribute('hidden');
        } else {
          table.setAttribute('hidden', true);
        }
      });
    });
}
