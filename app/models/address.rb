# == Schema Information
#
# Table name: addresses
#
#  id         :bigint           not null, primary key
#  city       :string(255)      not null
#  country    :string(255)      not null
#  serial     :string(36)       not null
#  street     :string(255)      not null
#  zip_code   :string(10)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_addresses_on_user_id  (user_id)
#

class Address < ApplicationRecord

  include Serialable

  validates :country,  presence: true, length: { maximum: 255 }
  validates :zip_code, presence: true, length: { maximum: 10 }
  validates :city,     presence: true, length: { maximum: 255 }
  validates :street,   presence: true, length: { maximum: 255 }
  validate :unique_attribute_set

  belongs_to :user, optional: true

  def unique_attribute_set
    if self.class.find_by(
      country:  country,
      zip_code: zip_code,
      city:     city,
      street:   street
    )
      errors.add(:address, 'has to be unique')
    end
  end

end
