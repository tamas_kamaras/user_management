module Serialable
  extend ActiveSupport::Concern

  included do
    before_create :set_serial
  end

  def set_serial
    self.serial = SecureRandom.uuid
  end
end
