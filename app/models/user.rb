# == Schema Information
#
# Table name: users
#
#  id         :bigint           not null, primary key
#  name       :string(255)      not null
#  serial     :string(36)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  manager_id :bigint
#
# Indexes
#
#  index_users_on_manager_id  (manager_id)
#

class User < ApplicationRecord

  include Serialable

  validates :name, presence: true, uniqueness: { case_sensitive: true }, length: { maximum: 255 }
  validates :manager, manager_relations: true, if: -> { manager.present? }

  has_many   :roles,     before_add: :force_unique_roles
  has_many   :addresses, before_add: :limit_addresses
  belongs_to :manager, class_name: 'User', foreign_key: :manager_id, optional: true
  accepts_nested_attributes_for :addresses

  class << self
    def managers
      joins(:roles).where(roles: { __type: 'manager' })
    end

    def non_managers
      where.not(id: User.pluck(:manager_id))
    end
  end

  def manager?
    roles.any? { |role| role.__type == 'manager' }
  end

  def force_unique_roles(role)
    if roles.pluck(:__type).include?(role_type = role.__type)
      raise(ActiveRecord::RecordInvalid.new, "\"#{name}\" user already has \"#{role_type}\" role")
    end
  end

  def limit_addresses(address)
    if addresses.size >= 5
      raise(ActiveRecord::RecordInvalid.new, 'A user can have maximum 5 addresses')
    end
  end

end
