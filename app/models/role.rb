# == Schema Information
#
# Table name: roles
#
#  id         :bigint           not null, primary key
#  __type     :string(10)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_roles_on_user_id  (user_id)
#

class Role < ApplicationRecord

  TYPES = [
    'guest',
    'manager',
    'developer'
  ]

  validates :__type, presence: true, inclusion: { in: TYPES }

  belongs_to :user

end
