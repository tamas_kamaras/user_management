class ManagerRelationsValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.is_a?(User) && !value.manager?
      record.errors.add(attribute, 'needs to have "manager" role')
    end
    if record.id == record.manager_id
      record.errors.add(attribute, 'can not be its own manager')
    end
    if record == record.manager.manager
      record.errors.add(attribute, '- A user can not be its own manager\'s manager')
    end
  end
end
