class UsersController < ApplicationController

  before_action :set_managers, only: [:new, :edit]

  def index
    @users        = User.all
    @non_managers = User.non_managers
  end

  def new
    @user = User.new
  end

  def create
    User.create!(normalized_params(create_user_params))
    redirect_to root_url, notice: 'A felhasználó sikeresen létrehozásra került!'
  rescue => error
    redirect_to root_url, alert: "A felhasználó létrehozása sikertelen! \nHibaüzenet: #{error.message}"
  end

  def edit
    @user = User.find_by(serial: params[:serial])
  end

  def update
    if user = User.find_by(serial: params[:serial])
      user.update!(normalized_params(user_params, user))
      redirect_to root_url, notice: 'A felhasználó sikeresen módosításra került!'
    else
      redirect_to root_url, alert: 'Ilyen felhasználó nem szerepel az adatbázisban!'
    end
  rescue => error
    redirect_to root_url, alert: "A felhasználó módosítása sikertelen! \nHibaüzenet: #{error.message}"
  end

  def set_managers
    @managers = User.managers.pluck(:name, :serial).to_h
  end

  private

  def user_params
    params.require(:user).permit(
      :name,
      :serial,
      :manager_serial,
      addresses_attributes: [
        :country,
        :zip_code,
        :city,
        :street
      ]
    )
  end

  def create_user_params
    user_params.except(:serial).permit!
  end

  def normalized_params(strong_params, user=nil)
    user_addresses = user&.addresses
    strong_params[:manager] = User.find_by(serial: strong_params.delete(:manager_serial))
    strong_params[:addresses_attributes]&.reject! do |address|
      address.values.uniq == [''] || user_addresses&.find_by(address)
    end
    strong_params
  end

end