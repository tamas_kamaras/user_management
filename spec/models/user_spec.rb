# == Schema Information
#
# Table name: users
#
#  id         :bigint           not null, primary key
#  name       :string(255)      not null
#  serial     :string(36)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  manager_id :bigint
#
# Indexes
#
#  index_users_on_manager_id  (manager_id)
#

require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:user_1) { described_class.create!(name: 'First User') }
  let(:user_2)  { described_class.create!(name: 'Second User') }

  describe 'ATTRIBUTES' do
    it { should have_db_column(:id)    .of_type(:integer).with_options(null: false) }
    it { should have_db_column(:serial).of_type(:string) .with_options(null: false, limit: 36) }
    it { should have_db_column(:name)  .of_type(:string) .with_options(null: false, limit: 255, uniqueness: true) }
  end

  describe 'VALIDATIONS' do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_most(255) }
    it { should validate_uniqueness_of(:name) }
  end

  describe 'ASSOCIATIONS' do
    it { should have_many(:roles) }
    it { should have_many(:addresses) }
    it { should belong_to(:manager).class_name('User').with_foreign_key(:manager_id).optional(:true) }
    it 'is expected to have uniq roles (each role only once)' do
      user_1.roles << Role.new(__type: 'guest')
      expect { user_1.roles << Role.new(__type: 'guest') }.to raise_error(
        ActiveRecord::RecordInvalid,
        /"First User" user already has "guest" role/
      )
    end
    it 'is expected to have maximum 5 addresses' do
      5.times do |n|
        user_1.addresses << Address.new(
          country:  'Hungary',
          zip_code: "112#{n}",
          city:     'Budapest',
          street:   'Alkotás'
        )
      end
      expect do
        user_1.addresses << Address.new(
          country:  'Hungary',
          zip_code: "1123",
          city:     'Debrecen',
          street:   'Alkotás'
        )
      end.to raise_error(
        ActiveRecord::RecordInvalid,
        'A user can have maximum 5 addresses'
      )
    end
  end

  describe 'INSTANCES' do
    context 'when no user have manager role' do
      it 'then setting manager raises error' do
        expect{ user_1.update!(manager: user_2) }.to raise_error(
          ActiveRecord::RecordInvalid,
          /Manager needs to have "manager" role/
        )
      end
    end

    context 'when a user has manager role' do
      before(:each) do
        user_1.roles << Role.new(__type: 'manager')
      end

      it 'then setting itself as a manager for itself raises error' do
        expect{ user_1.update!(manager: user_1) }.to raise_error(
          ActiveRecord::RecordInvalid,
          /Manager can not be its own manager/
        )
      end

      context 'and a user is the manager of another manager' do
        before(:each) do
          user_2.roles << Role.new(__type: 'manager')
          user_1.update!(manager: user_2)
        end

        it 'then setting the user\'s own manager\'s manager raises error' do
          expect { user_2.update!(manager: user_1) }.to raise_error(
            ActiveRecord::RecordInvalid,
            /- A user can not be its own manager\'s manager/
          )
        end
      end
    end
  end
end
