# == Schema Information
#
# Table name: addresses
#
#  id         :bigint           not null, primary key
#  city       :string(255)      not null
#  country    :string(255)      not null
#  serial     :string(36)       not null
#  street     :string(255)      not null
#  zip_code   :string(10)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_addresses_on_user_id  (user_id)
#

require 'rails_helper'

RSpec.describe Address, type: :model do
  describe 'ATTRIBUTES' do
    it { should have_db_column(:id)      .of_type(:integer).with_options(null: false) }
    it { should have_db_column(:serial)  .of_type(:string).with_options(null: false, limit: 36) }
    it { should have_db_column(:country) .of_type(:string).with_options(null: false, limit: 255) }
    it { should have_db_column(:zip_code).of_type(:string).with_options(null: false, limit: 10) }
    it { should have_db_column(:city)    .of_type(:string).with_options(null: false, limit: 255) }
    it { should have_db_column(:street)  .of_type(:string).with_options(null: false, limit: 255) }
    it { should have_db_column(:user_id) .of_type(:integer) }
  end

  describe 'VALIDATIONS' do
    it { should validate_presence_of(:country) }
    it { should validate_presence_of(:zip_code) }
    it { should validate_presence_of(:city) }
    it { should validate_presence_of(:street) }

    it { should validate_length_of(:country) .is_at_most(255) }
    it { should validate_length_of(:zip_code).is_at_most(10) }
    it { should validate_length_of(:city)    .is_at_most(255) }
    it { should validate_length_of(:street)  .is_at_most(255) }

    it 'is expected to validate that address is unique' do
      Address.create!(
        country:  'Hungary',
        zip_code: '1123',
        city:     'Budapest',
        street:   'Alkotás'
      )
      expect do
        Address.create!(
          country:  'Hungary',
          zip_code: '1123',
          city:     'Budapest',
          street:   'Alkotás'
        )
      end.to raise_error(
        ActiveRecord::RecordInvalid,
        /Address has to be unique/
      )
    end
  end

  describe 'ASSOCIATIONS' do
    it { should belong_to(:user).optional }
  end
end
