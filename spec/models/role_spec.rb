# == Schema Information
#
# Table name: roles
#
#  id         :bigint           not null, primary key
#  __type     :string(10)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_roles_on_user_id  (user_id)
#

require 'rails_helper'

RSpec.describe Role, type: :model do
  describe 'ATTRIBUTES' do
    it { should have_db_column(:id)     .of_type(:integer).with_options(null: false) }
    it { should have_db_column(:__type) .of_type(:string) .with_options(null: false, limit: 10) }
    it { should have_db_column(:user_id).of_type(:integer) }
  end

  describe 'ASSOCIATIONS' do
    it { should belong_to(:user) }
  end

  describe 'VALIDATIONS' do
    let(:default_user) { User.create!(name: 'Default') }

    it 'allows only permitted __types' do
      expect do
        described_class.create!(__type: 'random', user: default_user)
      end.to raise_error(
        ActiveRecord::RecordInvalid,
        /Type is not included in the list/
      )
      expect do
        described_class.create!(__type: 'guest', user: default_user)
      end.not_to raise_error
    end
  end

  it 'defines "TYPES" constant with right values' do
    expect(described_class::TYPES).to eq([
      'guest',
      'manager',
      'developer'
    ])
  end

end
