require 'rails_helper'

RSpec.describe 'User routings', type: :routing do
  let(:user) { User.create!(name: 'Felhasználó') }

  it 'routes / to users#index action' do
    expect(get('/')).to route_to('users#index')
  end

  it 'routes /felhasznalok to users#index action' do
    expect(get('/felhasznalok')).to route_to('users#index')
  end

  it 'routes /felhasznalok/uj to users#new action' do
    expect(get('/felhasznalok/uj')).to route_to('users#new')
  end

  it 'routes POST /felhasznalok to users#create action' do
    expect(post('/felhasznalok')).to route_to('users#create')
  end

  it 'routes /felhasznalo/:serial/szerkesztes to users#edit action' do
    expect(get("/felhasznalo/#{user.serial}/szerkesztes")).to route_to(
      controller: 'users',
      action:     'edit',
      serial:     user.serial
    )
  end

  it 'routes PATCH /felhasznalo/:serial to users#update action' do
    expect(patch("/felhasznalo/#{user.serial}")).to route_to(
      controller: 'users',
      action:     'update',
      serial:     user.serial
    )
  end
end