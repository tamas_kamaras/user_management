require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let!(:user) { User.create!(name: 'Felhasználó') }
  let!(:manager) do
    User.create!(
      name: 'Boss',
      roles: [Role.new(__type: 'manager')]
    )
  end
  let(:address) do
    Address.new(
      country:  'Hungary',
      zip_code: '1123',
      city:     'Budapest',
      street:   'Alkotás'
    )
  end
  let(:create_params) do
    {
      user: {
        name:           'Felhasználó 2',
        manager_serial: manager.serial,
        addresses_attributes: [{
          country:  address.country,
          zip_code: address.zip_code,
          city:     address.city,
          street:   address.street
        }]
      }
    }
  end
  let(:update_params) do
    {
      serial: user.serial,
      user: {
        name: 'Módosított Felhasználó'
      }
    }
  end

  describe '#index' do
    before(:each) do
      10.times do |n|
        user = User.create!(name: "Felhasználó #{n}")
        user.roles << Role.new(__type: Role::TYPES[rand(0..2)])
      end
      get(:index)
    end

    it 'assigns the collection of all users' do
      expect(assigns(:users)).to eq(User.all)
    end

    it 'assigns all non-managers' do
      expect(assigns(:non_managers)).to eq(User.where.not(id: User.pluck(:manager_id)))
    end

    it 'renders the "index" template' do
      expect(response).to render_template('index')
    end
  end

  describe '#new' do
    before(:each) do
      get(:new)
    end

    it 'assigns a blank User instance' do
      expect(assigns(:user)).to be_a(User)
      expect(assigns(:user)).not_to be_persisted
      expect(controller.instance_variable_get(:@user).attributes)
        .to eq(User.new.attributes)
    end

    it 'assigns all user with manager role' do
      expect(assigns(:managers)).to eq(User.managers.pluck(:name, :serial).to_h)
    end

    it 'renders the "new" template' do
      expect(response).to render_template('new')
    end
  end

  describe '#create' do
    subject { post(:create, params: create_params) }

    it 'creates a new user with a manager' do
      expect { subject }.to change { User.count }.by(1)
    end

    it 'the new user has a manager' do
      subject
      expect(User.find_by(name: create_params[:user][:name]).manager).to eq(manager)
    end

    it 'the new user has an address' do
      subject
      expect(User.find_by(name: create_params[:user][:name]).addresses).to include(Address.last)
    end

    it 'after creation it redirects to #index action' do
      expect(subject).to redirect_to(action: :index)
    end
  end

  describe '#edit' do
    before(:each) do
      get(:edit, params: { serial: user.serial } )
    end

    it 'assigns the requested user' do
      expect(assigns(:user)).to eq(user)
    end

    it 'assigns all user with manager role' do
      expect(assigns(:managers)).to eq(User.managers.pluck(:name, :serial).to_h)
    end

    it 'renders the "edit" template' do
      expect(response).to render_template('edit')
    end
  end

  describe '#update' do
    subject { patch(:update, params: update_params) }

    it 'updates an existing user' do
      expect { subject; user.reload }.to change { user.name }.from('Felhasználó').to(update_params[:user][:name])
    end

    it 'after update it redirects to #index action' do
      expect(subject).to redirect_to(action: :index)
    end
  end

  describe 'StrongParameters' do

    it 'permits users with its nested attributes' do
      should permit(
        :name,
        :serial,
        :manager_serial,
        addresses_attributes: [
          :country,
          :zip_code,
          :city,
          :street
        ]
      ).for(:create, verb: :post, params: create_params).on(:user)
    end
  end
end