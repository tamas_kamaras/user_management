Rails.application.routes.draw do
  root to: 'users#index'

  get '/felhasznalok', to: 'users#index'

  get  '/felhasznalok/uj', to: 'users#new'
  post '/felhasznalok',    to: 'users#create', as: :create_user

  get   '/felhasznalo/:serial/szerkesztes', to: 'users#edit'
  patch '/felhasznalo/:serial',             to: 'users#update', as: :update_user
end
