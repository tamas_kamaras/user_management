class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.belongs_to :user
      t.string     :serial,   null: false, limit: 36
      t.string     :country,  null: false, limit: 255
      t.string     :zip_code, null: false, limit: 10
      t.string     :city,     null: false, limit: 255
      t.string     :street,   null: false, limit: 255
      t.timestamps
    end
  end
end
