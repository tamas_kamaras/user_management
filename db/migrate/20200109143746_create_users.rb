class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.belongs_to :manager
      t.string :serial, null: false, limit: 36
      t.string :name,   null: false, limit: 255, unique: true
      t.timestamps
    end
  end
end
